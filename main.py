#!/usr/bin/env python
import sys

from app.__main__ import main

if __name__ == '__main__':
    main(sys.argv[1:])
