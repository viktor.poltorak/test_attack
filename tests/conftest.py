import pytest
import os

from sqlalchemy import create_engine
from sqlalchemy_utils import create_database, database_exists, drop_database
from starlette.testclient import TestClient

os.environ['ENV'] = 'test'
os.environ['DATABASE_URL'] = 'sqlite:///test_database.db'

from app.web import create_web_app
from app.tables import metadata
from app import settings


@pytest.fixture(scope='session')
def db_url() -> str:
    return str(settings.DATABASE_URL)


@pytest.fixture(autouse=True, scope='session')
def db_schema(db_url):
    engine = create_engine(db_url)
    if database_exists(db_url):
        drop_database(db_url)
    create_database(db_url)
    metadata.create_all(engine)
    yield
    drop_database(db_url)


@pytest.fixture()
def app():
    return create_web_app()


@pytest.fixture()
def client(app):
    return TestClient(app)
