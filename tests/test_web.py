import pytest

from app import services


@pytest.fixture()
async def test_data():
    await services.create_vm('vm_test1', 'test1', ['dest'])
    await services.create_vm('vm_test2', 'test2', ['source'])
    await services.create_fw_rule('fw_test', source_tag='source', dest_tag='dest')
    return True


@pytest.mark.asyncio
async def test_get_stats(client):
    response = client.get("/stats")
    assert response.json() == {'average_request_time': 0, 'request_count': 0, 'vm_count': await services.get_vm_count()}

    response = client.get("/stats")

    assert response.json()['request_count'] == 1
    assert response.json()['average_request_time'] > 0


@pytest.mark.asyncio
async def test_attack_with_no_param(client):
    response = client.get("/attack")
    assert response.status_code == 400


@pytest.mark.asyncio
async def test_attack(client, test_data):
    response = client.get("/attack?vm_id=vm_test1")
    assert response.json() == ['vm_test2']

    response = client.get("/attack?vm_id=vm_test2")
    assert response.json() == []
