import typing as t

from app import tables
from app.db import db
import sqlalchemy as sa


class VMNotFound(Exception):
    pass


def reset_schema():
    tables.drop_tables()
    tables.create_tables()


async def get_row_by_id(table: sa.Table, id_field: sa.Column, id_value: t.Union[int, str]) -> t.Optional[dict]:
    query = table.select().where(id_field == id_value)
    result = await db.fetch_one(query)
    return result


async def create_vm(vm_id: str, name: str, tags: list) -> t.Optional[dict]:
    vm = await get_row_by_id(tables.vms, tables.vms.columns.vm_id, vm_id)
    if not vm:
        query = tables.vms.insert().values(vm_id=vm_id, name=name)
        vm = await db.execute(query)
    for tag in tags:
        query = tables.vm_tag.insert().values(vm_id=vm_id, tag=tag)
        await db.execute(query)
    return vm


async def create_fw_rule(fw_id: str, source_tag: str, dest_tag: str) -> dict:
    fw = await get_row_by_id(tables.fw_rule, tables.fw_rule.columns.fw_id, fw_id)
    if not fw:
        query = tables.fw_rule.insert().values(fw_id=fw_id, source_tag=source_tag, dest_tag=dest_tag)
        fw = await db.execute(query)
    return fw


async def get_vm_count() -> int:
    query = sa.select([sa.func.count()]).select_from(tables.vms)
    result = await db.fetch_val(query)
    return result


async def get_attacking_vms_by_id(vm_id: str) -> list:
    """
    return a list of the virtual machine ids that can potentially attack it by vm_id

    the query:
    select * from vms _vms
    where _vms.vm_id in (
        select _vm_tag.vm_id
        from vm_tag _vm_tag
        where tag in (select _fw_rule.source_tag
                      from fw_rule _fw_rule
                      where _fw_rule.dest_tag in (select tag from vm_tag where vm_id = VM_ID))
    )
        and _vms.vm_id != VM_ID

    :param vm_id: str
    :return: list
    """

    vm = await get_row_by_id(tables.vms, tables.vms.columns.vm_id, vm_id)
    if not vm:
        raise VMNotFound

    subquery_dest_tag = sa.select([tables.vm_tag.columns.tag]).where(tables.vm_tag.columns.vm_id == vm_id)
    subquery_rule = sa.select([tables.fw_rule.columns.source_tag]).where(
        tables.fw_rule.columns.dest_tag.in_(subquery_dest_tag)
    )
    subquery_tags_by_rule = sa.select([tables.vm_tag.columns.vm_id]).where(tables.vm_tag.columns.tag.in_(subquery_rule))
    query = sa.select([tables.vms.columns.vm_id]).where(
        sa.and_(
            tables.vms.columns.vm_id.in_(subquery_tags_by_rule),
            tables.vms.columns.vm_id != vm_id
        )
    )
    result = await db.fetch_all(query)
    return [item[0] for item in result]
