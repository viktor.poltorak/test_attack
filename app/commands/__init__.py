import click
from .serve import serve
from .import_data import import_data

cli = click.Group()
cli.add_command(serve)
cli.add_command(import_data)
