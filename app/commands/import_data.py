import click
import json
import asyncio

from app import services


async def _import_vms(data: dict):
    for item in data:
        await services.create_vm(vm_id=item.get("vm_id"), name=item.get('name'), tags=item.get("tags"))


async def _import_fw_rules(data: dict):
    for item in data:
        await services.create_fw_rule(
            fw_id=item.get("fw_id"),
            source_tag=item.get("source_tag"),
            dest_tag=item.get("dest_tag")
        )


@click.command()
@click.argument('data_file', type=str)
@click.option("--clear", type=str, default=True)
def import_data(data_file: str, clear: bool, **kwargs):  # pragma: nocover
    with open(data_file) as fp:
        data = json.load(fp)

        if clear:
            services.reset_schema()

        asyncio.run(_import_vms(data.get("vms")))
        asyncio.run(_import_fw_rules(data.get("fw_rules")))
