import click
import uvicorn


@click.command()
@click.option("--host", type=str, default="0.0.0.0")
@click.option("--port", type=int, default="8080")
@click.option("--debug", type=bool, is_flag=True, default=False)
@click.option("--log-level", type=str, default="info")
def serve(**kwargs):  # pragma: nocover
    uvicorn.run("app.web:app", **kwargs)
