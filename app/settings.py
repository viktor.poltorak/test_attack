import os

from databases import DatabaseURL
from starlette.config import Config

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PKG_DIR = os.path.dirname(__file__)


def project_path(path: str) -> str:  # pragma: nocover
    """Return path to file relative to the project root."""
    return os.path.abspath(os.path.join(BASE_DIR, path))


# load settings local to the current machine. dev only!
dotenv_path = project_path(".env")
if not os.path.exists(dotenv_path):  # pragma: no cover
    dotenv_path = None

env = Config(dotenv_path)
DEBUG = env("DEBUG", bool, False)
ENV = env('ENV', str, 'prod')

DATABASE_URL: DatabaseURL = env(
    "DATABASE_URL", DatabaseURL,
    "sqlite:///database.db"
)
