import time
import logging

from starlette.applications import Starlette
from starlette.exceptions import HTTPException
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.routing import Route

from app import settings, services
from app.db import db
from app.services import VMNotFound
from app.tables import create_tables
from app.stats import stats


def request_stats(func):
    async def wrapped(*args, **kwargs):
        start_time = time.time()
        result = await func(*args, **kwargs)
        stats.add_request(time.time() - start_time)
        return result

    return wrapped


@request_stats
async def attack_view(request: Request):
    vm_id = request.query_params.get('vm_id')
    if not vm_id:
        logging.error("attack_view: vm_id empty")
        raise HTTPException(status_code=400, detail="parameter 'vm_id' is required")

    try:
        logging.info(f"Checking {vm_id}")
        result = await services.get_attacking_vms_by_id(vm_id=vm_id)
    except VMNotFound as e:
        raise HTTPException(status_code=404, detail=f'VM with {vm_id} not found')

    return JSONResponse(result)


@request_stats
async def stats_view(request: Request):
    return JSONResponse(
        {
            "vm_count": await services.get_vm_count(),
            "request_count": stats.get_request_count(),
            "average_request_time": stats.get_average_time()
        }
    )


async def on_app_startup():
    await db.connect()
    create_tables()


async def on_app_shutdown():
    await db.disconnect()


async def http_exception(request, exc):
    logging.exception(exc)
    return JSONResponse({"detail": exc.detail}, status_code=exc.status_code)


exception_handlers = {
    HTTPException: http_exception
}


def create_web_app() -> Starlette:
    routes = [
        Route("/attack", attack_view, methods=["GET"]),
        Route("/stats", stats_view, methods=["GET"])
    ]

    return Starlette(
        debug=settings.DEBUG,
        routes=routes,
        on_startup=[on_app_startup],
        on_shutdown=[on_app_shutdown],
        exception_handlers=exception_handlers,
    )


app = create_web_app()
