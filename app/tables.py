import sqlalchemy as sa
from .db import db

engine = sa.create_engine(str(db.url))

metadata = sa.MetaData()
metadata.bind = engine

vms = sa.Table(
    "vms",
    metadata,
    sa.Column(
        "vm_id",
        sa.String,
        index=True,
        primary_key=True,
    ),
    sa.Column("name", sa.String),
)

vm_tag = sa.Table(
    "vm_tag",
    metadata,
    sa.Column(
        "id",
        sa.Integer,
        index=True,
        primary_key=True,
        autoincrement=True,
    ),
    sa.Column("vm_id", sa.String, sa.ForeignKey("vms.vm_id", ondelete='CASCADE')),
    sa.Column("tag", sa.String, index=True),
)

fw_rule = sa.Table(
    "fw_rule",
    metadata,
    sa.Column(
        "fw_id",
        sa.String,
        index=True,
        primary_key=True,
    ),
    sa.Column("source_tag", sa.String, index=True),
    sa.Column("dest_tag", sa.String, index=True),
)


def create_tables():  # pragma: nocover
    metadata.create_all(engine)


def drop_tables():
    metadata.drop_all(engine)
