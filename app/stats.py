from app import services


class Stats:
    """
    Collect and provide statistic since the start of the process.
    """

    def __init__(self):
        self.request_count = 0
        self.total_time = 0

    def clear(self):
        self.request_count = 0
        self.total_time = 0

    def add_request(self, request_time: float):
        self.request_count += 1
        self.total_time += request_time

    def get_request_count(self) -> int:
        return self.request_count

    def get_average_time(self) -> float:
        return self.total_time / self.request_count if self.request_count else 0


stats = Stats()
