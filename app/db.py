from databases import Database

from app import settings

db = Database(settings.DATABASE_URL)
