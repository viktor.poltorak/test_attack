import logging
import sys

from app.commands import cli

logging.basicConfig(level=logging.INFO)


def main(args):  # pragma: nocover
    cli()


if __name__ == "__main__":  # pragma: nocover
    """Application entry point."""
    main(sys.argv[1:])
