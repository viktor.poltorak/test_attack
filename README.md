## Attack surface service - exercise

### Installation

```bash
poetry install
```

### Configuration

You can configure application using environment.

You can find required variables in .env.dist file. For more options consult app/settings.py file.

### Running

```bash
./main.py serve
```

### Cli

#### Import data

```bash
./main.py import-data ./data/input-0.json
```

### Test

```bash
pytest
```